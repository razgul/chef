== Documentation

Model structure

* RecipeCategory
	* type (breakfast, dinner, etc)

* Recipe
	* title
	* summary
	* instructions
	* recipeCategoryId 
	* timeToPrepare

* ProductCategory
	* type (spices, vegetables, etc)
a
* Product
	* name
	* productCategoryID
	* unitId

* Storage
	* productId
	* amount

* Unit
	* name (cup, gram etc)

* Ingredient
	* recipeId
	* productId
	* unitId
	* amount