class Recipe < ActiveRecord::Base
  belongs_to :recipe_category
  belongs_to :category, class_name: :recipe_category, foreign_key: "recipe_category_id"
  has_many :ingredients
  has_many :products, through: :ingredients

  accepts_nested_attributes_for :ingredients
end
