class Product < ActiveRecord::Base
  belongs_to :product_category
  belongs_to :category, class_name: :product_category, foreign_key: "product_category_id"
  belongs_to :unit
end
