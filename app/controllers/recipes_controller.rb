class RecipesController < ApplicationController

	def index
		@recipes = Recipe.all
	end

	def new
		@recipe = Recipe.new
		@recipe.ingredients.build
		@categories = RecipeCategory.all
		@products = Product.all
	end

	def create
		@recipe = Recipe.new(params.require(:recipe).permit(:title, :summary, :instructions))
		@recipe.recipe_category = RecipeCategory.find(params[:recipe][:category])

		if @recipe.save
			params[:recipe][:ingredient_ids].reject(&:blank?).each do |productId|
				@ingredient = Ingredient.new(recipe: @recipe, product: Product.find(productId), amount: 1)
				@ingredient.save
			end
			debugger
			redirect_to recipes_path
		else
			render 'new'
		end
	end
end
