class ProductsController < ApplicationController
  
  def index
  	@products = Product.all
  end

  def new
  	@product = Product.new
  	@categories = ProductCategory.all
  	@units = Unit.all
  end

  def create
  	@product = Product.new(params.require(:product).permit(:name))
  	@product.product_category = ProductCategory.find(params[:product][:category])
  	@product.unit = Unit.find(params[:product][:unit])
  	if @product.save
  		redirect_to products_path
  	else
  		render 'new'
  	end
  end

  def autocomplete
    @products = Product.order(:name).where("name LIKE ?", "%#{params[:term]}%")
    respond_to do |format|
      format.html
      format.json { 
        render json: @products.map(&:name)
      }
    end
  end



end
