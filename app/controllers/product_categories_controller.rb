class ProductCategoriesController < ApplicationController
 
  def index
  	@categories = ProductCategory.all
  end

  def new
  	@category = ProductCategory.new
  end

  def create
  	@category = ProductCategory.new(params.require(:product_category).permit(:name))
  	if @category.save
  		redirect_to product_categories_path
  	else
  		render 'new'
  	end
  end
  
end
