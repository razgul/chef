class UnitsController < ApplicationController
	
  def index
  	@units = Unit.all
  end

  def new
  	@unit = Unit.new
  end

  def create
  	@unit = Unit.new(params.require(:unit).permit(:name))
  	if @unit.save
  		redirect_to units_path
  	else
  		render 'new'
  	end
  end

end
