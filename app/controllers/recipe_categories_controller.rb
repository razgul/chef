class RecipeCategoriesController < ApplicationController
  
  def index
  	@recipeCategories = RecipeCategory.all
  end

  def new
  	@recipeCategory = RecipeCategory.new
  end

  def create
  	@recipeCategory = RecipeCategory.new(params.require(:recipe_category).permit(:name))
  	if @recipeCategory.save
  		redirect_to root_path
  	else
  		render 'new'
  	end
  end

  def edit
  end

end
