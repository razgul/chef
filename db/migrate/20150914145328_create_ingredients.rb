class CreateIngredients < ActiveRecord::Migration
  def change
    create_table :ingredients do |t|
      t.references :recipe, index: true
      t.references :product, index: true
      t.integer :amount

      t.timestamps null: false
    end
    add_foreign_key :ingredients, :recipes
    add_foreign_key :ingredients, :products
  end
end
