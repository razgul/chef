class AddUserToStorages < ActiveRecord::Migration
  def change
    add_reference :storages, :user, index: true
    add_foreign_key :storages, :users
  end
end
