class CreateStorings < ActiveRecord::Migration
  def change
    create_table :storings do |t|
      t.references :storage, index: true
      t.references :product, index: true
      t.integer :amount

      t.timestamps null: false
    end
    add_foreign_key :storings, :storages
    add_foreign_key :storings, :products
  end
end
