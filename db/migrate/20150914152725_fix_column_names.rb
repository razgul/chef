class FixColumnNames < ActiveRecord::Migration
  def change
  	rename_column :recipe_categories, :type, :name
  	rename_column :product_categories, :type, :name
  	rename_column :units, :type, :name
  end
end
