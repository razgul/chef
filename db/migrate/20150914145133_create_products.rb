class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.references :product_category, index: true
      t.references :unit, index: true

      t.timestamps null: false
    end
    add_foreign_key :products, :product_categories
    add_foreign_key :products, :units
  end
end
