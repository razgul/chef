class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :title
      t.text :summary
      t.text :instructions
      t.references :recipe_category, index: true
      t.integer :timeToPrepare

      t.timestamps null: false
    end
    add_foreign_key :recipes, :recipe_categories
  end
end
